<?php

namespace Drupal\webform_private_elements\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure webform admin settings for private elements.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * The webform element manager.
   *
   * @var \Drupal\webform\Plugin\WebformElementManagerInterface
   */
  protected $elementManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->elementManager = $container->get('plugin.manager.webform.element');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_admin_config_private_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['webform_private_elements.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('webform_private_elements.settings');

    /** @var \Drupal\webform\Plugin\WebformElementInterface[] $element_plugins */
    $element_plugins = $this->elementManager->getInstances();
    $options = [];
    foreach ($element_plugins as $plugin) {
      $id = $plugin->getPluginId();
      $options[$id] = $id . ' - ' . $plugin->getPluginLabel();
    }
    ksort($options);

    $default_value = $config->get('private') ?: [];
    if (!$default_value) {
      $default_value = array_fill_keys(array_keys($options), 0);
    }

    $form['private'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Private elements'),
      '#options' => $options,
      '#default_value' => $default_value,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $private = $form_state->getValue('private');
    $this->config('webform_private_elements.settings')->set('private', array_filter($private))->save();
  }

}
