The webform private elements module allows site administrators to define which webform
elements are "private" by default.

When these elements are added to a form, the "private" setting is automatically checked.

Private elements are used by webform to control additional access checks before showing
"private data".
